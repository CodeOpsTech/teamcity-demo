import boto3    
import json
import os  
def lambda_handler(event, context):    

    # extract the contents of the file
    content = {
        "message": "Your function executed successfully after the change!", 
    }


    # print the contents of the files
    # print(file_content)  
    return {"statusCode": 200, "body": json.dumps(content)}
